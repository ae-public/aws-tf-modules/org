This module is intended to serve as a basic AWS Organisation account constructor whilst waiting for TLZ to be published.

To access this module from within terraform, please see Ref:1 below.

References:
1. https://www.terraform.io/docs/modules/sources.html
