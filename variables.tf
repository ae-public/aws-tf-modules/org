variable "org_name" {
  description = "Name of Organisation"
  type        = string
}

variable "ou_name" {
  description = "Name of OU"
  type        = string
}

variable "acc_name" {
  description = "Account Name"
  type        = string
}

variable "org_email_prefix" {
  description = "Organisations email prefix"
  type        = string
}

variable "org_email_suffix" {
  description = "Organisations email suffix"
  type        = string
}

variable "tf_bucket" {
  description = "Terraform State S3 Bucket Name"
  type        = string
  default     = "tf_bucket"
}

variable "tf_dynamodb_table" {
  description = "Terraform State Lockiing DDB Table"
  type        = string
  default     = "tf_dynamodb_table"
}

variable "org_parent_id" {
  description = "Parent to bind account to - either an OU or parent account"
  type        = string
}

variable "aws_region" {
  type    = string
  default = "eu-west-1"
}

variable "central_log_bucket" {
  type = string
}

variable "log_bucket_name" {
  type = string
}

variable "create_log_bucket" {
  type    = bool
  default = false
}

variable "create_central_log_bucket" {
  type    = bool
  default = false
}

variable "tfstate_prep" {
  type    = bool
  default = false
}

variable "org_admin_role" {
  type    = string
  default = "OrgAdminRole"
}

variable "min_pw_length" {
  type    = number
  default = 12
}

variable "pw_history" {
  type    = number
  default = 6
}

variable "max_pw_age" {
  type    = number
  default = 180
}

variable "allow_security_acc" {
  type    = bool
  default = true
}

variable "is_security_acc" {
  type    = bool
  default = false
}

variable "org_security_acc_id" {
  type    = string
  default = ""
}

variable "org_member_ids" {
  type    = list(string)
  default = ["undefined"]
}

variable "tags" {
  description = "Map of tags to add to all resources"
  type        = map(string)
  default     = {}
}
