output "aws_organizations_account_id" {
  value = aws_organizations_account.this.id
}

output "s3_log_bucket" {
  value = concat(aws_s3_bucket.log_bucket.*.id, [""])[0]
}

output "s3_tfstate_bucket" {
  value = concat(aws_s3_bucket.tf_state.*.id, [""])[0]
}

output "central_log_bucket" {
  value = concat(aws_s3_bucket.central_log_bucket.*.id, [""])[0]
}
