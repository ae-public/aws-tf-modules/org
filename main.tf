provider "aws" {
  region = var.aws_region
}

provider "aws" {
  region = var.aws_region
  alias  = "that"
  assume_role {
    role_arn = "arn:aws:iam::${aws_organizations_account.this.id}:role/${var.org_admin_role}"
    #    session_name = join(":",[ var.org_name, var.ou_name, var.acc_name])
  }
}

resource "aws_organizations_account" "this" {
  name      = join(":", [var.org_name, var.ou_name, var.acc_name])
  email     = join("", [var.org_email_prefix, join("", ["+", lower(var.ou_name), "-", lower(var.acc_name)]), var.org_email_suffix])
  parent_id = var.org_parent_id
  role_name = var.org_admin_role
  tags = merge(
    local.tags,
    var.tags,
    map(
      "Name", join(":", [var.org_name, var.ou_name, var.acc_name]),
  ))
}

# TF:
# S3 Bucket
# DynamooDB

# Mgmt Roles

# Logging ->
resource "aws_s3_bucket" "log_bucket" {
  count    = var.create_log_bucket ? 1 : 0
  provider = aws.that
  bucket   = var.log_bucket_name
  acl      = "log-delivery-write"
  tags = merge(
    local.tags,
    var.tags,
    map(
      "Name", lower(join(":", [var.org_name, var.ou_name, var.acc_name, "log-bucket"])),
  ))

  versioning {
    enabled = true
  }
}


resource "aws_s3_bucket" "central_log_bucket" {
  count    = var.create_central_log_bucket ? 1 : 0
  provider = aws.that
  bucket   = var.central_log_bucket
  acl      = "log-delivery-write"
  tags = merge(
    local.tags,
    var.tags,
    map(
      "Name", lower(join(":", [var.org_name, var.ou_name, var.acc_name, "log-bucket"])),
  ))

  versioning {
    enabled = true
  }
}

resource "aws_s3_bucket" "tf_state" {
  count    = var.tfstate_prep ? 1 : 0
  provider = aws.that
  bucket   = lower(join("-", [var.org_name, var.ou_name, var.acc_name, "tf-state"]))
  acl      = "private"
  tags = merge(
    local.tags,
    var.tags,
    map(
      "Name", lower(join(":", [var.org_name, var.ou_name, var.acc_name, "tf-state"])),
  ))

  versioning {
    enabled = true
  }

  logging {
    target_bucket = var.log_bucket_name
    target_prefix = "log/"
  }
}

# IAM Accounts in secAcc 


# https://www.terraform.io/docs/configuration/providers.html
