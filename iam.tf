locals {
  org_sec_acc = "${var.org_security_acc_id == "" ? aws_organizations_account.this.id : var.org_security_acc_id}"
  policy      = templatefile("${path.module}/policy_gsa.tmpl", { org_security_acc_id = local.org_sec_acc, org_member_ids = var.org_member_ids })
}

resource "aws_iam_account_password_policy" "strict" {
  provider                       = aws.that
  minimum_password_length        = var.min_pw_length
  password_reuse_prevention      = var.pw_history
  require_lowercase_characters   = true
  require_numbers                = true
  require_uppercase_characters   = true
  require_symbols                = true
  allow_users_to_change_password = true
  hard_expiry                    = true
  max_password_age               = var.max_pw_age
}

resource "aws_iam_account_alias" "alias" {
  provider      = aws.that
  account_alias = lower(join("-", [var.org_name, var.ou_name, var.acc_name]))
}

# IAM Groups granting access TO remote accounts
resource "aws_iam_group" "GlobalSuperAdmin" {
  count    = var.is_security_acc ? 1 : 0
  provider = aws.that
  name     = "GlobalSuperAdmin"
}

resource "aws_iam_group" "GlobalReadOnly" {
  count    = var.is_security_acc ? 1 : 0
  provider = aws.that
  name     = "GlobalReadOnly"
}

resource "aws_iam_group_policy" "GlobalSuperAdmin" {
  name     = "GlobalSuperAdminPolicy"
  provider = aws.that
  count    = var.is_security_acc ? 1 : 0
  group    = concat(aws_iam_group.GlobalSuperAdmin.*.id, [""])[0]
  policy   = local.policy
}

resource "aws_iam_group_policy" "GlobalReadOnly" {
  name     = "GlobalReadOnlyPolicy"
  provider = aws.that
  count    = var.is_security_acc ? 1 : 0
  group    = concat(aws_iam_group.GlobalReadOnly.*.id, [""])[0]
  policy   = local.policy
}

# IAM Roles granting access FROM remote accounts
resource "aws_iam_role" "SecGroupAdmin" {
  count    = var.allow_security_acc ? 1 : 0
  provider = aws.that
  name     = "SecGroupAdmin"
  tags = merge(
    local.tags,
    var.tags,
    map(
      "Name", join(":", [var.org_name, var.ou_name, var.acc_name, "SecGroupAdmin"]),
  ))
  assume_role_policy = <<-EOF
  {
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": [ "arn:aws:iam::${local.org_sec_acc}:root" ]
      },
      "Action": "sts:AssumeRole",
      "Condition": {}
    }
  ]
  }
  EOF
}

resource "aws_iam_role" "SecGroupReadOnly" {
  count    = var.allow_security_acc ? 1 : 0
  provider = aws.that
  name     = "SecGroupReadOnly"
  tags = merge(
    local.tags,
    var.tags,
    map(
      "Name", join(":", [var.org_name, var.ou_name, var.acc_name, "SecGroupReadOnly"]),
  ))
  assume_role_policy = <<-EOF
  {
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": [ "arn:aws:iam::${local.org_sec_acc}:root" ]
      },
      "Action": "sts:AssumeRole",
      "Condition": {}
    }
  ]
  }
  EOF
}

# IAM Group Policy granting access to remote accounts
resource "aws_iam_policy_attachment" "global_sa" {
  provider   = aws.that
  count      = var.allow_security_acc ? 1 : 0
  name       = "global_sa"
  roles      = [aws_iam_role.SecGroupAdmin[0].name]
  policy_arn = data.aws_iam_policy.super_admin.arn
}

resource "aws_iam_policy_attachment" "global_ro" {
  provider   = aws.that
  count      = var.allow_security_acc ? 1 : 0
  name       = "global_ro"
  roles      = [aws_iam_role.SecGroupReadOnly[0].name]
  policy_arn = data.aws_iam_policy.ec2_global_ro.arn
}

# IAM Policies - AWS Managed
data "aws_iam_policy" "ec2_global_ro" {
  provider = aws.that
  arn      = "arn:aws:iam::aws:policy/ReadOnlyAccess"
}

data "aws_iam_policy" "super_admin" {
  provider = aws.that
  arn      = "arn:aws:iam::aws:policy/AdministratorAccess"
}
